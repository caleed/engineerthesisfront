import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarComponent } from './components/calendar/calendar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SuggestionsComponent } from './components/suggestions/suggestions.component';
import { HolderComponent } from './holder/holder.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { ProgresComponent } from './progressions/progres/progres.component';
import { StatisticsComponent } from './statistics/statistics.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {path: 'home' , component: DashboardComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'suggestions', component: SuggestionsComponent},
  {path: 'progress', component: ProgresComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'holder', component: HolderComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
