import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ExerciseDto } from 'src/app/model/exerciseDto';
import { TagDto } from 'src/app/model/tagDto';
import { WrapperLists } from 'src/app/model/wrapperLists';
import { DefaultExercisesControllerService } from 'src/app/services/defaultExercisesController.service';
import { ExerciseControllerService } from 'src/app/services/exercise-controller.service';
import { TagControllerService } from 'src/app/services/tagController.service';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.scss']
})
export class SuggestionsComponent implements OnInit {

  tags: TagDto[] = [];
  dropdownList = [];
  selectedItems;
  dropdownSettings: IDropdownSettings;
  exercises: ExerciseDto[];
  photo: any;
  tagsChecked: TagDto[] = [];
  generatedExercises: ExerciseDto[] = [];
  defaultExercises:any[] = [];
  hidden: boolean;


  constructor(private tagService: TagControllerService, private exerciseService: ExerciseControllerService,
    private defaultExercisesService: DefaultExercisesControllerService) {
  }


  ngOnInit(): void {
    this.loadTags();
    this.dropdownList = this.tags;
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };
  }

  loadTags(): void {
    this.tagService.getAllUsingGET().subscribe((tag) => {
      this.tags = tag;
    })
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  search() {
    return this.exerciseService.suggestUsingGET(this.selectedItems.map(s => s.id)).subscribe(t => {
      this.exercises = t;
    })
  }

  display() {
    this.exerciseService.getPhotoUsingGET("eks.png").subscribe(t => {
      this.photo = t;
    })
  }

  getTag(e, tag){
    if(e){
      this.tagsChecked.push(tag)
      console.log(this.tagsChecked)
    }
    else
    {
      let index = this.tagsChecked.indexOf(tag);
      this.tagsChecked.splice(index, 1);
      console.log(this.tagsChecked);
    }
  }

  generateTraining(){
    this.hidden = true;
     this.defaultExercisesService.generateExerciseUsingGET(this.tagsChecked.map(s => s.id)).subscribe(t => {
      this.defaultExercises = t;
    })

  }


 




}
