import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Configuration } from 'src/app/configuration';
import { AuthControllerService } from 'src/app/services/auth-controller.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  emailInput;
  passwordInput;
  firstNameInput;
  lastNameInput;
  genderInput;


  constructor(private authControllerService : AuthControllerService, private configuration : Configuration, private router : Router) { }

  ngOnInit(): void {
  }

  registerUser(){
    return this.authControllerService.registerUsingPOST({
      email : this.emailInput,
      firstName : this.firstNameInput,
      gender : this.genderInput,
      lastName: this.lastNameInput,
      password: this.passwordInput
    }).subscribe(t => {
      localStorage.setItem('currenUser', JSON.stringify(t))
      this.configuration.apiKeys = {
        "Authorization" : t.token
      }
      this.router.navigate(['/statistics'])
    }, err => {
      alert("TRY AGAIN!");
    });
  }

}
