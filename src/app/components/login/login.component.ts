import { Component, OnInit } from '@angular/core';
import { AuthControllerService } from 'src/app/services/auth-controller.service';
import { Router } from '@angular/router'
import { Configuration } from 'src/app/configuration';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public passwordInput: string;
  public nameInput: string;

  constructor(private authControllerService: AuthControllerService, private router : Router, private configuration: Configuration) { }

  ngOnInit(): void {
  }

  loginUser() {
    return this.authControllerService.loginUsingPOST({
      email: this.nameInput,
      password: this.passwordInput
    }).subscribe(t => {
      console.log(t)
      localStorage.setItem('currenUser', JSON.stringify(t))
      this.configuration.apiKeys = {
        "Authorization" : t.token
      }
      this.router.navigate(['/statistics'])
    }, err => {
      alert("TRY AGAIN!");
    });
  }

}
