

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { NewTraining } from 'src/app/model/newTraining';
import { TrainingDto } from 'src/app/model/trainingDto';
import { Wrapper } from 'src/app/model/wrapper';
import { ExerciseControllerService } from 'src/app/services/exercise-controller.service';
import { TrainingControllerService } from 'src/app/services/training-controller.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor(private trainingService: TrainingControllerService,
    private exerciseService: ExerciseControllerService,
    private formBuilder: FormBuilder
  ) { }

  trainings: TrainingDto[] = [];
  hidden: boolean[] = [];
  newTraining: NewTraining;
  add: boolean;
  naz;
  exercisesA;
  editboo: boolean;
  editerT: TrainingDto;
  i;
  allUserExercises: Wrapper[];
  
  activeTrainings: TrainingDto[] = [];
  active: boolean [] = [];


  nowyFormularz : FormGroup

  ngOnInit(): void {
    this.loadTreinings();
    this.nowyFormularz = this.budowaFormularza();
    this.getUserExerciseToSuggest();
  }

  passTaken(ind){
    this.activeTrainings.push(this.trainings[ind]);
    console.log(this.activeTrainings);
    this.exerciseService.setActiveTrainings(this.activeTrainings);
  }

  budowaFormularza(){
    return this.formBuilder.group({
      name : "",
      exercises : this.formBuilder.array([])
    })
  }


  budowaCwiczen() : FormArray{
    return (<FormArray>this.nowyFormularz.get('exercises')) as FormArray
  }

  noweCwiczenie() : FormGroup{
    return this.formBuilder.group({
      name: '',
      sets: this.formBuilder.array([])
    })
  }

  dodajCwiczenie(){
    this.budowaCwiczen().push(this.noweCwiczenie());
  }

  usunCwiczenie(empIndex:number) {
    this.budowaCwiczen().removeAt(empIndex);
  }


  CwiczenieSerie(empIndex:number) : FormArray {
    return this.budowaCwiczen().at(empIndex).get("sets") as FormArray
  }

  nowaSeria(): FormGroup {
    return this.formBuilder.group({
      weight: '',
      repActual: '',
      sortOrder: ''
    })
  }

  dodajSerie(empIndex:number) {
    this.CwiczenieSerie(empIndex).push(this.nowaSeria());
  }

  usunSerie(empIndex:number,skillIndex:number) {
    this.CwiczenieSerie(empIndex).removeAt(skillIndex);
  }


 


  showSubItem(ind) {
    this.hidden[ind] = !this.hidden[ind];
  }

  loadTreinings() {
    return this.trainingService.getAllTrainingsByUserIdUsingGET().subscribe(t => {
      this.trainings = t
      console.log(t)
    }, err => {
      alert("Nie posiadasz treningów!")
    })
  }

  pobierz() {
    this.nowyFormularz.reset();
    this.clearFormArray();
    this.naz = this.nowyFormularz.controls['name'].value;
    this.exercisesA = this.budowaCwiczen().value;
    this.trainingService.addTrainingUsingPOST({
      name: this.naz,
      exercises: this.exercisesA
    }).subscribe(t => {
      console.log(t);
      this.loadTreinings();
    }, er => console.log(er))
  }

  dodaj() {
    this.add = !this.add;
  }

  getOneTraning(ind) {
    this.editboo = !this.editboo;
    this.editerT = this.trainings[ind];
    this.i = this.trainings[ind].id

    this.nowyFormularz.reset();
    this.clearFormArray();

    this.editerT.exercises.forEach(t =>{
      var cw : FormGroup = this.noweCwiczenie();
      this.budowaCwiczen().push(cw);

      t.sets.forEach(b =>{
        var se  = this.nowaSeria();

        (cw.get('sets') as FormArray).push(se);
      });

    });

    this.nowyFormularz.patchValue(this.editerT);

  }


  clearFormArray(){
    this.budowaCwiczen().clear();
  }


  edit(): any
  {
    this.naz = this.nowyFormularz.controls['name'].value;
    this.exercisesA = this.budowaCwiczen().value;
    this.trainingService.editTrainingUsingPUT(this.i, {
      name : this.naz,
      exercises :  this.exercisesA
    }).subscribe(t => {
      this.loadTreinings();
    }, er => console.log(er))
  }

  deleteTraining(ind){
    this.editerT = this.trainings[ind];

    this.trainingService.deleteTrainingUsingDELETE(this.editerT.id).subscribe(t => {
      this.loadTreinings();
    }, er => console.log(er))
  }

  getUserExerciseToSuggest() {
    return this.exerciseService.suggestExerciseUsingGET().subscribe(t =>{
      this.allUserExercises = t;
    })
  }

}
