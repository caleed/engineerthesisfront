import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { AuthControllerService } from './services/auth-controller.service';
import { ExerciseControllerService } from './services/exercise-controller.service';
import { SetsControllerService } from './services/sets-controller.service';
import { TrainingControllerService } from './services/training-controller.service';
import { UserControllerService } from './services/user-controller.service';
import { TagControllerService } from './services/tagController.service';
import { DefaultExercisesControllerService } from './services/defaultExercisesController.service';
import { SizesControllerService } from './services/sizesController.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    AuthControllerService,
    ExerciseControllerService,
    SetsControllerService,
    TrainingControllerService,
    UserControllerService,
    TagControllerService,
    DefaultExercisesControllerService,
    SizesControllerService
 ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): any {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
                 
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
