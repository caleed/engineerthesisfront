import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgresComponent } from './progres.component';
import { ChartsModule } from 'ng2-charts';
import { SidebarModule } from 'src/app/shared/sidebar/sidebar.module';
//import { ChartsModule } from 'node_modules/chart.js'



@NgModule({
  exports : [ProgresComponent],
  declarations: [ProgresComponent],
  imports: [
    CommonModule,
    ChartsModule,
    SidebarModule
  ]
})
export class ProgresModule { }
