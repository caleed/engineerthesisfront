import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ExerciseDto } from 'src/app/model/exerciseDto';
import { SetsDto } from 'src/app/model/setsDto';
import { TrainingDto } from 'src/app/model/trainingDto';
import { Wrapper } from 'src/app/model/wrapper';
import { ExerciseControllerService } from 'src/app/services/exercise-controller.service';
import { SetsControllerService } from 'src/app/services/sets-controller.service';
import { Chart } from 'chart.js'

@Component({
  selector: 'app-progres',
  templateUrl: './progres.component.html',
  styleUrls: ['./progres.component.scss']
})
export class ProgresComponent implements OnInit {

  constructor(private exerciseService: ExerciseControllerService, private setsService: SetsControllerService,) { }

  activeTrainings: TrainingDto[] = [];
  exercises: ExerciseDto[];
  sets: SetsDto[] = [];
  allUserExercises: Wrapper[];
  hidden: boolean[] = [];
  rm;
  charts: any;



  ngOnInit(): void {
    this.loadExercises();
    this.getUserExerciseToSuggest();
    this.activeTrainings = this.exerciseService.getActiveTrainings();
  };

  loadExercises() {
    this.exerciseService.getUserExercisesUsingGET().subscribe(t => {
      this.exercises = t;
    })
  }

 async get1RM(name, i) {
  this.rm = await this.setsService.getOneMaxUsingGET(name).toPromise()
    this.showSubItem(name, i);
  }

  
  getUserExerciseToSuggest() {
    return this.exerciseService.suggestExerciseUsingGET().subscribe(t => {
      this.allUserExercises = t;
    })
  }

  showSubItem(name, ind) {
  
    this.showGraph(name, ind);
  }

  showGraph(name, ind) {
   
    this.hidden.forEach(element => {
      this.hidden.fill(false)
    });
    this.hidden[ind] = !this.hidden[ind];
    this.setsService.getSetsForNameUsingGET(name).subscribe(t => {
  
      this.sets = t;
      let data = this.sets.map(res => (res.weight / this.rm));
      let dats = this.sets.map(res => res.repActual);

      this.charts = new Chart('canvas', {
        type: 'line',
        data: {
          labels: dats,
          datasets: [
            {
              data: data,
              backgroundColor: 'blue',
              fill: false
            },
          ]
        }, options: {
          legend: {
            display: false,
          },
          scales: {
            xAxes: [{
              display: true,

            }],
            yAxes: [{
              display: true,
            }],
          }
        }
      })
    })
  }
}
