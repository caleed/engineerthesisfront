import { SetsDto } from './setsDto';
import { TrainingDto } from './trainingDto';

export interface NewExercise { 
    name: string;
    sets: Array<SetsDto>;
    training: TrainingDto;
}