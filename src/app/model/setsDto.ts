export interface SetsDto { 
    id?: number;
    repActual?: number;
    sortOrder?: number;
    weight?: number;
}