export interface UserDto { 
    email?: string;
    firstName?: string;
    gender?: UserDto.GenderEnum;
    id?: number;
    lastName?: string;
    role?: UserDto.RoleEnum;
    weight?: number;
}
export namespace UserDto {
    export type GenderEnum = 'Female' | 'Male' | 'Other';
    export const GenderEnum = {
        Female: 'Female' as GenderEnum,
        Male: 'Male' as GenderEnum,
        Other: 'Other' as GenderEnum
    };
    export type RoleEnum = 'ADMIN' | 'TRAINER' | 'USER';
    export const RoleEnum = {
        ADMIN: 'ADMIN' as RoleEnum,
        TRAINER: 'TRAINER' as RoleEnum,
        USER: 'USER' as RoleEnum
    };
}