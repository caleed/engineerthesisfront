import { TagDto } from './tagDto';
import { SetsDto } from './setsDto';

export interface ExerciseDto { 
    id?: number;
    name?: string;
    tagDto?: TagDto;
    sets?: Array<SetsDto>;
    trainingId?: number;
}