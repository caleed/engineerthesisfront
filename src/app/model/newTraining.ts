import { ExerciseDto } from './exerciseDto';
export interface NewTraining { 
    name: string;
    exercises?: Array<ExerciseDto>;
}