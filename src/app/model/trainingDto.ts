import {UserDto} from './userDto'
import { ExerciseDto } from './exerciseDto';

export interface TrainingDto { 
    id?: number;
    name?: string;
    userDto?: UserDto;
    exercises?: Array<ExerciseDto>;
}