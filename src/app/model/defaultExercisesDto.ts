import { SetsDto } from './setsDto';
import { TagDto } from './tagDto';

export interface DefaultExercisesDto { 
    id?: number;
    name?: string;
    sets?: Array<SetsDto>;
    tagDto?: Array<TagDto>;
}