import { LoginComponent } from '../components/login/login.component';

export interface TagDto { 
    id: number;
    multiplier?: number;
    name?: string;
}