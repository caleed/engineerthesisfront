import { UserDto } from './userDto';

export interface AuthResponse { 
    token?: string;
    userDto?: UserDto;
}