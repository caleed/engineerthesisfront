import { UserDto } from './userDto';

export interface SizesDto { 
    bodyParts?: SizesDto.BodyPartsEnum;
    size?: number;
    user?: UserDto;
    diff?: number;
}
export namespace SizesDto {
    export type BodyPartsEnum = 'Arm' | 'Calf' | 'Chest' | 'Forearm' | 'Shoulders' | 'Thigh' | 'Waist';
    export const BodyPartsEnum = {
        Arm: 'Arm' as BodyPartsEnum,
        Calf: 'Calf' as BodyPartsEnum,
        Chest: 'Chest' as BodyPartsEnum,
        Forearm: 'Forearm' as BodyPartsEnum,
        Shoulders: 'Shoulders' as BodyPartsEnum,
        Thigh: 'Thigh' as BodyPartsEnum,
        Waist: 'Waist' as BodyPartsEnum
    };
}