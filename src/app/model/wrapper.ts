import { TagDto } from './tagDto';

export interface Wrapper { 
    name?: string;
    tags?: Array<TagDto>;
}