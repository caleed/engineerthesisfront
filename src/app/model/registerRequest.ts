export interface RegisterRequest { 
    email: string;
    firstName?: string;
    gender?: RegisterRequest.GenderEnum;
    lastName?: string;
    password: string;
}
export namespace RegisterRequest {
    export type GenderEnum = 'Female' | 'Male' | 'Other';
    export const GenderEnum = {
        Female: 'Female' as GenderEnum,
        Male: 'Male' as GenderEnum,
        Other: 'Other' as GenderEnum
    };
}