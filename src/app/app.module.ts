import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { SidebarModule } from './shared/sidebar/sidebar.module';
import { ApiModule } from './api.module';
import { Configuration, ConfigurationParameters } from './configuration';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ProgresModule } from './progressions/progres/progres.module';
import { StatisticsModule } from './statistics/statistics.module';
import { HolderModule } from './holder/holder.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

export function apiConfigFactory (): Configuration  {

  const user = JSON.parse(localStorage.getItem('currenUser'));

  if (user != null){
    return new Configuration({apiKeys:{ "Authorization" : user.token}});
  }
  return  new Configuration({});
}

export function rootLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, 'assets/i18n/','.json')
}


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    AppRoutingModule,
    HttpClientModule,
    DragDropModule,
    CommonModule,
    SidebarModule,
    ApiModule.forRoot(apiConfigFactory),
    NgMultiSelectDropDownModule.forRoot(),
    ProgresModule,
    StatisticsModule,
    HolderModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: rootLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
