import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  logged: boolean

  constructor(private router : Router) { }

  ngOnInit(): void {
    this.checkLogged();
  }

  checkLogged(){
    if(localStorage.length > 0)
    this.logged = true;
    else 
    this.logged = false;
  }

  logout(){
    localStorage.removeItem("currenUser");
    this.router.navigate(["/home"]);
  }

}
