import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsComponent } from './statistics.component';
import { ChartsModule } from 'ng2-charts';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  exports: [StatisticsComponent],
  declarations: [StatisticsComponent],
  imports: [
    CommonModule,
    ChartsModule,
    SidebarModule,
    TranslateModule,
    MatListModule,
    MatCheckboxModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    BrowserAnimationsModule
  ]
})
export class StatisticsModule { }
