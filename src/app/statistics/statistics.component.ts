import { Component, Directive, OnInit } from '@angular/core';
import { SizesDto } from '../model/sizesDto';
import { ExerciseControllerService } from '../services/exercise-controller.service';
import { SizesControllerService } from '../services/sizesController.service';
import { Chart } from 'chart.js'
import { GraphData } from '../model/graphData';
import { TrainingDto } from '../model/trainingDto';
import { TranslateConfigService } from '../services/translateConfig.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { UserControllerService } from '../services/user-controller.service';
import { SetsControllerService } from '../services/sets-controller.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})


export class StatisticsComponent implements OnInit {

  userSizes: SizesDto[] = [];
  usersExercises: any[] = [];
  charts: any
  lab: any[] = []
  tagsName: GraphData[] = [];
  trainings: TrainingDto[] = [];
  user;
  RepMax: any[] = [];
  tmp: any[] = [];
  begin;
  end;
  userWeight

  constructor(private userService: UserControllerService, private setsService: SetsControllerService, private TranslateConfigService: TranslateConfigService, private sizeService: SizesControllerService, private exerciseService: ExerciseControllerService) { }

  ngOnInit(): void {
    this.loadExercises();
    this.loadSizes();
    this.loadTagsNameFromExercise();
    this.getUserMail();
  }

  searchForSizes(){
    const current = new Date(this.begin);
    current.setHours(0)
    current.setMinutes(0)
    current.setSeconds(0)
    current.setMilliseconds(0)
    const timestamp1 = current.getTime()

    const current2 = new Date(this.end);
    current2.setHours(0)
    current2.setMinutes(0)
    current2.setSeconds(0)
    current2.setMilliseconds(0)
    const timestamp2 = current2.getTime()

    this.sizeService.getRecentUserSizesUsingGET(timestamp1, timestamp2).subscribe(t => {
      this.userSizes = t;
    })
  }

  getUserMail() {
    this.user = JSON.parse(localStorage.getItem('currenUser'));
    this.user = this.user.userDto.email;
    this.userWeight = JSON.parse(localStorage.getItem('currenUser'));
    this.userWeight = this.userWeight.userDto.weight;
  }

  generatePDF() {

    var tableNames = this.userSizes.map(t => t.bodyParts)
    var tableSizes = this.userSizes.map(t => t.size)
    var tableDiff = this.userSizes.map(t => t.diff)
    var canvasEquity = document.getElementById("canvas") as HTMLCanvasElement;
    var imgEquity = canvasEquity.toDataURL("image/png");
    var exercisesName = this.usersExercises.map(t => t.name)


    const documentDefinition = {
      content: [
        {
          text: "Podsumowanie dla " + this.user,
          style: 'header'
        },
        {
          text: "waga " + this.userWeight + " kg "
        },
        {
          table: {
            headerRows: 1,
            body: [
              tableNames,
              tableSizes,
              tableDiff
            ],
            style: 'tables'
          }
        },
        {
          image: imgEquity,
          width: 300,
          height: 300,
          styles: 'chart'
        },
        {
          table: {
            body: [
              [exercisesName, this.tmp.map(t => t.rm)]
            ]
          }
        }
      ],
      styles: {
        header: {
          margin: [0, 0, 0, 50],
          fontSize: 18,
          bold: true,
          alignment: "center",
        },
        tables: {
          margin: [0, 10, 0, 30],
          fontSize: 12,
        },
        chart: {
          alignment: 'center'
        }
      }
    };
    pdfMake.createPdf(documentDefinition).open();
  }

  async loadTagsNameFromExercise() {
    this.tagsName = await this.exerciseService.getTagsFromUserExercisesUsingGET().toPromise()
    this.showGraph();
  }

  loadExercises() {
    this.exerciseService.getUserExercisesUsingGET().subscribe(t => {
      this.usersExercises = t;
      this.usersExercises.forEach(element => {
        // this.RepMax.push(this.setsService.getOneMaxUsingGET(element.name)).s
        this.setsService.getOneMaxUsingGET(element.name).subscribe(k => {
          this.RepMax.push(k);
          this.tmp.push({ "name": element.name, "rm": k })
        })
      });
    })
  }

  loadSizes() {
    const current = new Date();
    current.setHours(0)
    current.setMinutes(0)
    current.setSeconds(0)
    current.setMilliseconds(0)
    const timestamp = current.getTime()
    console.log("timestamp: ", timestamp);
    

    this.sizeService.getRecentUserSizesUsingGET(0, timestamp).subscribe(t => {
     this.userSizes = t;
    })

  }

  save(bodyPart, sizeValue) {
    this.sizeService.addUserSizeUsingPOST({
      bodyParts: bodyPart,
      size: sizeValue
    }).subscribe(t => {
      console.log(t);
      this.loadSizes();
    }, err => console.log(err))
  }

  showGraph() {
    this.charts = new Chart('canvas', {
      type: 'pie',
      data: {
        labels: this.tagsName.map(t => t.tagName),
        datasets: [{
          data: this.tagsName.map(t => t.count2),
          backgroundColor: [
            'orange',
            'green',
            'red',
            'blue',
            'cyan',
            'brown',
            'Indigo'
          ],
        }]
      },
      options: {
        legend: {
          labels: {
            fontColor: 'gray',
            fontStyle: 'bold'
          }
        }
      }
    })
  }



}
