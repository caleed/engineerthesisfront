import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from '../app-routing.module'
import { FormsModule, ReactiveFormsModule} from '@angular/forms'
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { MatSelectModule } from '@angular/material/select';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BrowserModule } from '@angular/platform-browser';
import { CalendarComponent } from '../components/calendar/calendar.component';
import { SuggestionsComponent } from '../components/suggestions/suggestions.component';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
  

@NgModule({
  exports: [DashboardComponent, LoginComponent, RegisterComponent, CalendarComponent, SuggestionsComponent],
  declarations: [DashboardComponent, LoginComponent, RegisterComponent, CalendarComponent, SuggestionsComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule,
    MatSelectModule,
    DragDropModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule,
    MatListModule,
    MatCheckboxModule,
    SidebarModule
  ]
  
})
export class HomeModule { }
