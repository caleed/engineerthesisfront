import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HolderComponent } from './holder.component';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { RouterModule } from '@angular/router';



@NgModule({
  exports: [HolderComponent],
  declarations: [HolderComponent],
  imports: [
    CommonModule,
    SidebarModule,
    RouterModule,
  ]
})
export class HolderModule { }
